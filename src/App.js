import './App.css';
import './component/SideBar.css';

import Header from './component/Header';
import Sidebar from './component/SideBar';
import Dashboard from './component/Dashoard';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Customers from './component/Customers';
import Invoices from './component/Invoices';
import Pickup from './component/Pickup';
import Package from './component/Package';
import Return from './component/Return';
import CreateInvoices from './component/CreateInvoices';


function App() {
  return (
  <>
  <BrowserRouter>
  <Sidebar />
  <Header fixed="top"/>
  <Routes>
  {/* <Route  path='/' element={<Header/>} />
  <Route  path='/' element={<Sidebar/>} /> */}
  <Route  path='/dashboard' element={<Dashboard/>} />
    <Route  path='/customers' element={<Customers/>} />
    <Route path='/package' element={<Package/>} />
    <Route path='/invoices' element={<Invoices/>} />
    <Route path='/create' element={<CreateInvoices/>} />
    <Route path='/return' element={<Return/>} />
    <Route path='/pickup' element={<Pickup/>} />




  </Routes>
  </BrowserRouter>
  
  </>
  
  );
}

export default App;
