export default function Return(){
return(
<div className="main">

    <div className="page-wrapper">
  <div className="page-content">
    <div className="card mb-4">
      <div className="card-header">send a return message</div>
      <div className="card-body">
        <form method="POST" action="" encType="multipart/form-data">
          <div className="row">
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                 
                  Customer Name:
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input
                    name="cname"
                    defaultValue=""
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>
            </div>
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                 
                  phone:
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input
                    name="phone"
                    defaultValue=""
                    type="number"
                    className="form-control"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                  Total:
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input
                    name="total"
                    defaultValue="{{ old('total') }}"
                    type="number"
                    className="form-control"
                  />
                </div>
              </div>
            </div>
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                  Date create :
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input className="form-control" type="date" name="date" />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                  City:
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input className="form-control" type="text" name="city" />
                </div>
              </div>
            </div>
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                 
                  Quantity :
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <input
                    className="form-control"
                    type="Number"
                    name="quantity"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="mb-3 row">
                <label
                  htmlFor="state"
                  className="col-lg-2 col-md-6 col-sm-12 col-form-label"
                >
                  State
                </label>
                <br />
                <select
                  className="form-select form-control"
                  id="state"
                  name="state"
                >
                  <option
                    value="invoiced
						  "
                  >
                    Invoiced
                  </option>
                  <option value="pay">pay</option>
                  <option value="canceled">Canceled</option>
                </select>
              </div>
            </div>
            <div className="col">
              <div className="mb-3 row">
                <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                  Description
                </label>
                <div className="col-lg-10 col-md-6 col-sm-12">
                  <textarea
                    className="form-control"
                    id=""
                    rows={3}
                    defaultValue={""}
                  />
                </div>
              </div>
            </div>
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>


)
}
